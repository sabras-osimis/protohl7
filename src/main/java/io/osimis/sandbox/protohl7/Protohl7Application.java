package io.osimis.sandbox.protohl7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Protohl7Application {

	public static void main(String[] args) {
		SpringApplication.run(Protohl7Application.class, args);
	}

}
