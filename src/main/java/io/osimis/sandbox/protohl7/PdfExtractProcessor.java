package io.osimis.sandbox.protohl7;

import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.util.Terser;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import java.util.stream.Stream;

public class PdfExtractProcessor implements Processor {
    @Override
    public void process(Exchange exchange) throws Exception {
        final var input = exchange.getIn().getBody(Message.class);
        final var terser = new Terser(input);

        System.out.println(input.printStructure());

        Stream.of(
                terser.get("MSH-9-1"),
                terser.get("MSH-9-2"),
                terser.get("/RESPONSE/ORDER_OBSERVATION/ORC-2")
        ).forEach(System.out::println);

        System.out.println();

        final var pdfBase64 = terser.get("/RESPONSE/ORDER_OBSERVATION/OBSERVATION/OBX-5");

        System.out.println(pdfBase64);

        exchange.setProperty("report", pdfBase64);

        exchange.getIn().setBody(input.generateACK());
    }
}
