package io.osimis.sandbox.protohl7;

import org.apache.camel.component.hl7.HL7MLLPCodec;
import org.apache.camel.component.hl7.HL7MLLPNettyDecoderFactory;
import org.apache.camel.component.hl7.HL7MLLPNettyEncoderFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {
    @Bean
    HL7MLLPNettyDecoderFactory hl7decoder() {
        return new HL7MLLPNettyDecoderFactory();
    }

    @Bean
    HL7MLLPNettyEncoderFactory hl7encoder() {
        return new HL7MLLPNettyEncoderFactory();
    }

    @Bean
    HL7MLLPCodec hl7codec() {
        return new HL7MLLPCodec();
    }
}
