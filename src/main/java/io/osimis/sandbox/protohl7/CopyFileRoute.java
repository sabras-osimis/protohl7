package io.osimis.sandbox.protohl7;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.processor.aggregate.UseLatestAggregationStrategy;
import org.springframework.stereotype.Component;

@Component
public class CopyFileRoute extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        // NOTE : the file **MUST** be CRLF and not only LF,
        // otherwise parsing will either fail completely (exception) or partially (missing segment)

        from("file:///home/sa/tmp/camel/inbox?delete=true")
                .log("Receiving HL7 from file")
                .to("direct:hl7in");

        from("netty:tcp://localhost:8888?sync=true&encoders=#hl7encoder&decoders=#hl7decoder")
                .log("Receiving HL7 from MLLP")
                .to("direct:hl7in");

        from("direct:hl7in")
                .log("unmasrhalling")
                .unmarshal().hl7()
                .log("unmarshalled")
                .process(new PdfExtractProcessor())
                .marshal().hl7()
                .multicast(new UseLatestAggregationStrategy(), false)           // note: this is the default, but better explicitly define it
                .to("direct:savePdf", "direct:hl7ack");

        from("direct:savePdf")
                .setBody(exchange -> exchange.getProperty("report"))
                .unmarshal().base64()
                .to("file:///home/sa/tmp/camel/report?filename=report-${date:now:yyyyMMddhhmmss}.pdf");

        from("direct:hl7ack")
                .log("ACK");

    }
}
